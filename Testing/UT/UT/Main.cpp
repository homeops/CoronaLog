#include "Tests.hpp"

bool run_tests()
{
	bool total_result = true; 

	std::wcout << L"Running Tests: " << std::endl;

	for (auto& test : tests)
	{
		std::wcout << L"Running Test " << std::get<0>(test) << " ... ";
		
		bool result = std::get<1>(test)();
		
		total_result &= result;

		std::wcout << (result ? L"Success" : L"Failure") << std::endl;
	}

	return total_result;
}

int main()
{	
	if (!run_tests())
	{
		return 1;
	}

	return 0;
}