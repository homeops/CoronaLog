#pragma once

#include <iostream>
#include <string>
#include <functional>
#include <vector>
	
#define DEFINE_TEST(test_function_name) {L#test_function_name, test_function_name}

using TestFunction = std::function<bool(void)>;
using TestTuple = std::tuple<std::wstring, TestFunction>;