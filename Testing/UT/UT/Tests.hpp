#include "MiniUT.hpp"
#include "Common\Common.hpp"
#include "GeraLogger/GeraLogger.hpp"

bool geraTest1()
{
	try
	{
		GeraLogger::LogDebug(L"This is a test log");
	}
	catch (...)
	{
		return false;
	}

	return true;
}

bool geraTest2()
{
	try
	{
		GeraLogger::LogError(L"This is a test log");
	}
	catch (...)
	{
		return false;
	}

	return true;
}

std::vector<TestTuple> tests({ 
	DEFINE_TEST(geraTest1),
	DEFINE_TEST(geraTest2)
});
