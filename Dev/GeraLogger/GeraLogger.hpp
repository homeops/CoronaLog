#pragma once
#include <iostream>
#include <string>

/** 
 * Gera Logger a logger with my name as a prefix
 * Stolen from lagzi
**/
class GeraLogger {
public:
    static void LogError(std::wstring message);
    static void LogDebug(std::wstring message);
    
private:
    static void Log(std::wstring message);
};
