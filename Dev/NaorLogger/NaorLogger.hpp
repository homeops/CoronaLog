#pragma once

#include <Windows.h>
#include <string>

/**
 * \brief Awesome log class that creates a log file in a given path and appends log messages to it.
 */
class NaorLogger
{
public:
	explicit NaorLogger(std::wstring file_path);
	~NaorLogger();

	NaorLogger(NaorLogger&) = delete;
	NaorLogger& operator=(NaorLogger& other) = delete;

	NaorLogger(NaorLogger&&) = default;
	NaorLogger& operator=(NaorLogger&& other) = default;

	/**
	 * \brief Append a log to the log file.
	 * \param log_message Log message to write.
	 */
	void log(std::wstring log_message);

private:
	static HANDLE _s_open_log_file(std::wstring file_path);

	HANDLE m_log_file;
};