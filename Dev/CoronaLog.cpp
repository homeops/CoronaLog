#include "Common/Common.hpp"
#include "GeraLogger/GeraLogger.hpp"
#include "LagziLogger.hpp"
#include "ZLogger/ZLogger.hpp"
#include "NaorLogger/NaorLogger.hpp"

int wmain(int argc, WCHAR* argv[])
{
    if (argc <= 1)
    {
        std::cout << "Usage: <program> <log_file_path>" << std::endl;
        return 1;
    }

    std::cout << ("start logging!") << std::endl;

    GeraLogger::LogDebug(L"This is a debug log");

    LagziLogger::LogDebug(L"This is a debug log");

    ZLogger::log(L"This is a debug log");

	NaorLogger naor_logger(L"C:\\temp\\log.txt");
	naor_logger.log(L"This is a debug log");

    std::cout << ("stop logging!") << std::endl;
    return 0;
}
