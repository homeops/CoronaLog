#pragma once
#include <iostream>
#include <string>

/*
 * The best logger, ever.
 *
 * Features:
 * 		it can run Crysis
 * 		3rd generation big data interactive dynamic logging.
 * 		it's like, really fast
 * 		it's VERY cool
*/
class LagziLogger {
public:
    static void LogError(std::wstring message);
    static void LogDebug(std::wstring message);
    
private:
    static void Log(std::wstring message);
};
