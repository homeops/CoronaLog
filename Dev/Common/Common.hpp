#pragma once

#include <Windows.h>
#include <string>
#include <iostream>

void print(const std::wstring& data);

template<typename T>
void print(const T& data) 
{
	std::cout << data << std::endl;
}